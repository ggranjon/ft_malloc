#include "malloc.h"

void	*calloc(size_t num, size_t size)
{
    void	*ret_ptr;

    if (!(ret_ptr = malloc(num * size)))
        return (NULL);
    ft_bzero(ret_ptr, num * size);
    return (ret_ptr);
}